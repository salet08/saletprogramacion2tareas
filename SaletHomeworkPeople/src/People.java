public class People{
    private int age;
    private String name;
    private String lastName;
    private String GREET="hello";

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getGREET() {
        return GREET;
    }

    public String greet(){
        return GREET + " my name is " + name + " " + lastName;
    }

}
