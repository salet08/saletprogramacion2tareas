# SaletTareasProgramacionII.1



## Autor: Salet Yasmin Gutierrez Nava
En la presente tarea se resolvio de una manera el array donde se tiene que sumar
los numeros tanto impares como pares, donde esten consecutivamente y si no
lo estan entonces solo se copia el numero y asi contando cuantos numeros al final
del array quedan, lo cual se comprobo en los test y corrieron satisfactoriamente.
Ademas se añadio un main para que se pueda comprobar cuanto de longitud
se tiene en el final del arreglo.

Las instrucciones fueron las siguientes:

+ Dada una matriz de números enteros, suma números pares consecutivos y números impares consecutivos. Repita el proceso mientras se pueda hacer y devuelva la longitud de la matriz final.

Ejemplo:
```
Para arr = [2, 1, 2, 2, 6, 5, 0, 2, 0, 5, 5, 7, 7, 4, 3, 3, 9]
```
El resultado debería ser 6.
```
[2, 1, 2, 2, 6, 5, 0, 2, 0, 5, 5, 7, 7, 4, 3, 3, 9]  -->
2+2+6       0+2+0     5+5+7+7       3+3+9
[2, 1,   10,    5,    2,        24,     4,   15   ] -->
2+24+4
[2, 1,   10,    5,             30,           15   ]
The length of final array is 6
```
Con sus tes que son los siguientes:

```
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.runners.JUnit4;

public class SolutionTest {
    @Test
    public void basicTests() {
        assertEquals(6, Solution.sumGroups(new int[] {2, 1, 2, 2, 6, 5, 0, 2, 0, 5, 5, 7, 7, 4, 3, 3, 9}));
        assertEquals(5, Solution.sumGroups(new int[] {2, 1, 2, 2, 6, 5, 0, 2, 0, 3, 3, 3, 9, 2}));
        assertEquals(1, Solution.sumGroups(new int[] {2}));
        assertEquals(2, Solution.sumGroups(new int[] {1,2}));
        assertEquals(1, Solution.sumGroups(new int[] {1,1,2,2}));
    }
}
```
Para conseguir la solucion se dio un metodo para que de la solucion.

Lo que me dificulto mas fue en como se sumaria los numeros consecutivos
y ver si es que los numeros son pares o no pero que no se sumen con los demas
sino con lo de su al lado.

Y por ultimo se podria refactorizar un poco mas los metodos para que tenga 
un poco más de orden sin embargo por ahora lo puse de esta manera por 
el tiempo y no tenia otra idea de como poder refactorizarlo.