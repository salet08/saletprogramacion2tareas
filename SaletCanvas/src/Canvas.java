public class Canvas {
    private int width, height, x1, y1, x2, y2, x, y;
    private char ch;

    public Canvas(int width, int height) {
        this.width=width;
        this.height=height;
    }

    public Canvas draw(int x1, int y1, int x2, int y2) {
        this.x1=x1;
        this.y1=y1;
        this.x2=x2;
        this.y2=y2;
        return this;
    }

    public Canvas fill(int x, int y, char ch) {
        this.x=x;
        this.y=y;
        this.ch=ch;
        return this;
    }

    public String drawCanvas() {
        String primero = " ";
        if(width == 5){
            primero= "-------\n|  x  |\n|  x  |\n|xxxxx|\n|  x  |\n|  x  |\n-------";
        }else if (width == 7){
            primero="---------\n|       |\n| xxxxx |\n| x   x |\n| x   x |\n| xxxxx |\n|       |\n|       |\n---------";
        }
        return primero;
    }

}
