# SaletTareasProgramacionII

Author: Salet Yasmin Gutiérrez Nava

In this task it was seen that an interface had to be implemented where we had to call another class so that they have their respective methods, which was implemented in the task, a class "MysteryColorAnalyzerImpl" was created, according to the order that was given in the task and the implement of the class with its respective methods was added, where its respective methods were developed.
the next task is:
```
import java.util.List;

public interface MysteryColorAnalyzer {
    /**
     * This method will determine how many distinct colors are in the given list
     *
     * <p>
     * Colors are defined via the {@link Color} enum.
     * </p>
     *
     * @param mysteryColors list of colors from which to determine the number of distinct colors
     * @return number of distinct colors
     */
    int numberOfDistinctColors(List<Color> mysteryColors);

    /**
     * This method will count how often a specific color is in the given list
     *
     * <p>
     * Colors are defined via the {@link Color} enum.
     * </p>
     *
     * @param mysteryColors list of colors from which to determine the count of a specific color
     * @param color color to count
     * @return number of occurrence of the color in the list
     */
    int colorOccurrence(List<Color> mysteryColors, Color color);
}
Color has the following enum structure, but hey you really don't need to know all these colors if you're implementing the methods properly.

public enum Color {
    RED, GREEN, BLUE
}
```
As for the difficulties, there were not many, only with the first method, which was difficult at first but later it could be done, after the second, there were not many problems.
