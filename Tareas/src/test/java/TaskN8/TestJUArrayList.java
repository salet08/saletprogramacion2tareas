import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertArrayEquals;

public class TestJUArrayList {
    JUArrayList juArrayList = new JUArrayList();


    @Test
    public void testSize(){
        assertEquals(0,juArrayList.size());
        juArrayList.add(4);
        juArrayList.add(2);
        juArrayList.add(1);
        juArrayList.add(9);
        assertEquals(4,juArrayList.size());
        juArrayList.add(4);
        juArrayList.add(2);
        juArrayList.add(1);
        juArrayList.add(9);
        juArrayList.add(10);
        juArrayList.add(7);
        juArrayList.add(20);
        juArrayList.add(80);
        assertEquals(12,juArrayList.size());
    }

    @Test
    public void testIsEmpty(){
        assertFalse(juArrayList.isEmpty());
        juArrayList.add(40);
        juArrayList.add(29);
        juArrayList.add(45);
        juArrayList.add(100);
        juArrayList.add(1);
        assertTrue(juArrayList.isEmpty());
    }

    @Test
    public void testAdd(){
        assertArrayEquals(new Integer[]{}, juArrayList.getArr());
        juArrayList.add(6);
        juArrayList.add(2);
        juArrayList.add(82);
        juArrayList.add(90);
        juArrayList.add(92);
        juArrayList.add(100);
        assertArrayEquals(new Integer[]{6,2,82,90,92,100}, juArrayList.getArr());
        assertTrue(juArrayList.add(6));
    }

    @Test
    public void testRemoveObject(){
        Integer num1=11;
        Integer num2=12;
        Integer num3=13;
        Integer num4=14;
        Integer num5=15;
        juArrayList.add(num1);
        juArrayList.add(num2);
        juArrayList.add(num3);
        juArrayList.add(num4);
        juArrayList.add(num5);
        assertArrayEquals(new Integer[]{11,12,13,14,15}, juArrayList.getArr());
        juArrayList.remove(num2);
        assertArrayEquals(new Integer[]{11,13,14,15}, juArrayList.getArr());
    }

    @Test
    public void testClear(){
        assertArrayEquals(new Integer[]{}, juArrayList.getArr());
        juArrayList.add(1);
        juArrayList.add(2);
        juArrayList.add(3);
        juArrayList.add(4);
        juArrayList.add(5);
        juArrayList.add(6);
        juArrayList.add(7);
        juArrayList.add(8);
        juArrayList.add(9);
        assertArrayEquals(new Integer[]{1,2,3,4,5,6,7,8,9}, juArrayList.getArr());
        juArrayList.clear();
        assertArrayEquals(new Integer[]{0,0,0,0,0,0,0,0,0}, juArrayList.getArr());
    }

    @Test
    public void testGet(){
        juArrayList.add(2);
        juArrayList.add(3);
        assertArrayEquals(new Integer[]{2}, new Integer[]{juArrayList.get(0)});

    }

    @Test
    public void testRemoveIndex(){
        Integer num1=21;
        Integer num2=22;
        Integer num3=23;
        Integer num4=24;
        Integer num5=25;
        juArrayList.add(num1);
        juArrayList.add(num2);
        juArrayList.add(num3);
        juArrayList.add(num4);
        juArrayList.add(num5);
        assertArrayEquals(new Integer[]{21,22,23,24,25}, juArrayList.getArr());
        juArrayList.remove(num2);
        assertArrayEquals(new Integer[]{21,23,24,25}, juArrayList.getArr());
    }


    @Test
    public void testContains(){
        juArrayList.setArr(new Integer[]{1,2,3,6,4,56,9,7});
        assertTrue(juArrayList.contains(1));
        assertFalse(juArrayList.contains(34));
        assertTrue(juArrayList.contains(56));
        assertFalse(juArrayList.contains(100));
        assertTrue(juArrayList.contains(9));
        assertTrue(juArrayList.contains(2));
        assertFalse(juArrayList.contains(234));
        assertTrue(juArrayList.contains(3));
        assertFalse(juArrayList.contains(190));
        assertTrue(juArrayList.contains(6));
    }
}
