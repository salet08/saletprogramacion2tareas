import org.testng.annotations.Test;

import static org.testng.AssertJUnit.*;

public class DoublyCircularLinkedListTestInteger {
    @Test
    public void testSizeInteger(){
        List<Integer> list = new DoublyCircularLinkedList<>();
        assertEquals(0, list.size());

        list.add(10);
        list.add(11);
        list.add(12);
        list.add(13);
        list.add(14);

        assertEquals(5, list.size());

        list.add(15);
        list.add(16);
        list.add(17);
        list.add(18);
        list.add(19);

        assertEquals(10, list.size());
    }

    @Test
    public void testIsEmptyInteger(){
        List<Integer> list = new DoublyCircularLinkedList<>();
        assertTrue(list.isEmpty());

        list.add(101);
        list.add(112);
        list.add(123);
        list.add(134);
        list.add(145);

        assertFalse(list.isEmpty());
    }

    @Test
    public void testGetInteger(){
        List<Integer> list = new DoublyCircularLinkedList<>();

        list.add(3);
        list.add(8);
        list.add(5);
        list.add(9);
        list.add(2);

        Object number1 = 2;
        Object number2 = 8;
        Object number3 = 9;

        assertEquals(number1, list.get(4));
        assertEquals(number2, list.get(1));
        assertEquals(number3, list.get(3));

    }

    @Test
    public void testReverseInteger() {
        List<Integer> list1 = new DoublyCircularLinkedList<>();
        List<Integer> list2 = new DoublyCircularLinkedList<>();
        List<Integer> list3 = new DoublyCircularLinkedList<>();
        List<Integer> list4 = new DoublyCircularLinkedList<>();
        List<Integer> list5 = new DoublyCircularLinkedList<>();
        list1.add(0);
        list1.add(1);
        list1.add(2);
        list1.add(3);
        list1.add(4);
        assertArrayEquals(new Integer[]{0, 1, 2, 3, 4}, list1.toArray());
        list1.reverse();
        assertArrayEquals(new Integer[]{4, 3, 2, 1, 0}, list1.toArray());

        list2.add(10);
        list2.add(20);
        list2.add(30);
        list2.add(40);
        list2.add(50);
        list2.add(60);
        list2.add(70);
        assertArrayEquals(new Integer[]{10, 20, 30, 40, 50, 60, 70}, list2.toArray());
        list2.reverse();
        assertArrayEquals(new Integer[]{70, 60, 50, 40, 30, 20, 10}, list2.toArray());

        list3.add(100);
        list3.add(111);
        list3.add(222);
        list3.add(333);
        list3.add(444);
        assertArrayEquals(new Integer[]{100, 111, 222, 333, 444}, list3.toArray());
        list3.reverse();
        assertArrayEquals(new Integer[]{444, 333, 222, 111, 100}, list3.toArray());

        list4.add(12);
        assertArrayEquals(new Integer[]{12}, list4.toArray());
        list4.reverse();
        assertArrayEquals(new Integer[]{12}, list4.toArray());

        list5.add(0);
        list5.add(1);
        assertArrayEquals(new Integer[]{0, 1}, list5.toArray());
        list5.reverse();
        assertArrayEquals(new Integer[]{1, 0}, list5.toArray());
    }
}
