import org.testng.annotations.Test;

import static org.testng.AssertJUnit.*;

public class TestStackString {

    @Test
    public void testIsEmptyString(){
        Stack<String> list = new Stack<>();
        assertTrue(list.isEmpty());
        list.push("1");
        list.push("2");
        list.push("3");
        list.push("4");
        list.push("5");
        assertFalse(list.isEmpty());
        list.pop();
        list.pop();
        list.pop();
        list.pop();
        list.pop();
        assertTrue(list.isEmpty());
        list.push("David");
        assertFalse(list.isEmpty());
        list.pop();
        assertTrue(list.isEmpty());
        list.push("Diego");
        list.push("Maria");
        list.push("Jefersson");
        assertFalse(list.isEmpty());
        list.pop();
        list.pop();
        list.pop();
        assertTrue(list.isEmpty());
        list.push("Gaby");
        list.push("Reyes");
        list.push("Midoria");
        list.push("Izuku");
        assertFalse(list.isEmpty());
        list.pop();
        list.pop();
        list.pop();
        list.pop();
        assertTrue(list.isEmpty());
    }

    @Test
    public void testIsPushString(){
        Stack<String> list = new Stack<>();
        assertEquals(0, list.size());

        list.push("Maria");
        list.push("Jose");
        list.push("Teo");
        list.push("Carmen");
        assertEquals(4, list.size());

        list.push("Shoto");
        assertEquals(5, list.size());

        list.push("Ash");
        list.push("Eiji");
        list.push("Baji");
        assertEquals(8, list.size());

        list.push("Micke");
        list.push("Mario");
        assertEquals(10, list.size());
    }

    @Test
    public void testIsPopString(){
        Stack<String> list = new Stack<>();
        assertEquals(0, list.size());
        list.push("Pelota");
        list.push("Arbol");
        list.push("Mesa");
        list.push("Silla");
        assertFalse(list.isEmpty());
        assertEquals(4, list.size());

        assertFalse(list.isEmpty());
        list.pop();
        assertEquals(3, list.size());

        list.push("A");
        list.push("B");
        list.push("C");
        assertEquals(6, list.size());

        list.pop();
        assertEquals(5, list.size());
        list.push("D");
        list.push("E");
        list.push("F");
        list.push("G");
        list.push("H");
        assertEquals(10, list.size());

        list.pop();
        assertEquals(9, list.size());

        list.pop();
        list.pop();
        list.pop();
        list.pop();
        list.pop();
        list.pop();
        assertEquals(3, list.size());

        list.pop();
        list.pop();
        list.pop();
        assertTrue(list.isEmpty());
    }
}
