import org.testng.annotations.Test;

import static org.testng.AssertJUnit.*;

public class TestStackInteger {
        @Test
        public void testIsEmptyInteger() {
            Stack<Integer> list = new Stack<>();
            assertTrue(list.isEmpty());
            list.push(1);
            list.push(2);
            list.push(3);
            list.push(4);
            list.push(5);
            assertFalse(list.isEmpty());
            list.pop();
            list.pop();
            list.pop();
            list.pop();
            list.pop();
            assertTrue(list.isEmpty());
            list.push(6);
            assertFalse(list.isEmpty());
            list.pop();
            assertTrue(list.isEmpty());
            list.push(7);
            list.push(8);
            list.push(9);
            assertFalse(list.isEmpty());
            list.pop();
            list.pop();
            list.pop();
            assertTrue(list.isEmpty());
            list.push(10);
            assertFalse(list.isEmpty());
        }

        @Test
        public void testPushInteger(){
            Stack<Integer> list = new Stack<>();
            assertTrue(list.isEmpty());
            list.push(100);
            list.push(120);
            list.push(130);
            assertEquals(3, list.size());
            list.push(140);
            assertEquals(4, list.size());
            list.push(150);
            list.push(160);
            list.push(170);
            list.push(180);
            list.push(190);
            assertEquals(9, list.size());
            list.push(1100);
            assertEquals(10, list.size());
            assertFalse(list.isEmpty());
        }


        @Test
        public void testIsPopInteger(){
            Stack<Integer> list = new Stack<>();
            assertTrue(list.isEmpty());

            list.push(12);
            list.push(23);
            list.push(45);
            list.push(67);
            list.push(89);
            assertEquals(5, list.size());
            list.pop();
            assertEquals(4, list.size());
            list.push(13);
            list.push(25);
            list.push(36);
            assertEquals(7, list.size());
            list.pop();
            list.pop();
            assertEquals(5, list.size());
            list.pop();
            assertEquals(4, list.size());
            list.pop();
            list.pop();
            list.pop();
            list.pop();
            assertEquals(0, list.size());
        }
}
