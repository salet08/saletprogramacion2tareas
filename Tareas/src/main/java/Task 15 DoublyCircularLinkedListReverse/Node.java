public class Node <T>{
    private T data;
    private Node<T> next, previous;

    public Node(T data) {
        this.data = data;
        next = previous = null;
    }

    public void setPrevious(Node<T> previous) {
        this.previous = previous;
    }

    public T getData() {
        return data;
    }

    public Node<T> getNext() {
        return next;
    }

    public void setNext(Node<T> node) {
        next = node;
    }

    public String toString() {
        return "{data=" + data + ", sig->" + "}";
    }

}
