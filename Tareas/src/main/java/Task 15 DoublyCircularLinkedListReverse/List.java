public interface List<T> extends Comparable<T> {

    int size();

    boolean isEmpty();

    T get(int index);

    void reverse();

    boolean add(T data);

    Object[] toArray();

}