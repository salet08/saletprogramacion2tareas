class LinkedBag<T extends Comparable<T> > implements Bag<T> {
    private int size;
    private Node<T> root;

    public boolean add(T data) {
        Node<T> node = new Node<>(data);
        node.setNext(root);
        root = node;
        size++;

        return true;
    }

    public String toString() {
        return "(" + size + ")" + "root=" + root;
    }


    private <T extends Comparable<T>> boolean compare(T first, T second) {
        return first.compareTo(second) > 0;
    }

    public void selectionSort() {
        for (int i = 0; i < size - 1; i++) {
            int indexMin = i;
            for (int j = i + 1; j < size; j++) {
                if (compare(get(indexMin), get(j))) {
                    indexMin = j;
                }
            }
            xchange(indexMin, i);
        }
    }

    public void bubbleSort() {
        boolean sorted;
        do {
            sorted = true;
            for (int i = 0; i < size - 1; i++) {
                if (compare(get(i), get(i + 1))) {
                    xchange(i, i+1);
                    sorted  = false;
                }
            }
        } while (!sorted);
    }

    private T get(int index) {
        Node<T> node = root;
        for (int i = 0; i < index; i++, node = node.getNext()) ;
        return node.getData();
    }

    @Override
    public void xchange(int y, int x) {
        if (x == y)
            return;
        Node<T> nodeX = null, currentX = root;
        int i = 0;
        while (currentX != null && i != x) {
            nodeX = currentX;
            currentX = currentX.getNext();
            i++;
        }
        Node<T> nodeY = null, currentY = root;
        i = 0;
        while (currentY != null && i != y) {
            nodeY = currentY;
            currentY = currentY.getNext();
            i++;
        }
        if (currentX == null || currentY == null)
            return;
        if (nodeX != null)
            nodeX.setNext(currentY);
        else
            root = currentY;
        if (nodeY != null)
            nodeY.setNext(currentX);
        else
            root = currentX;
        Node<T> temp = currentX.getNext();
        currentX.setNext(currentY.getNext());
        currentY.setNext(temp);
    }

}


