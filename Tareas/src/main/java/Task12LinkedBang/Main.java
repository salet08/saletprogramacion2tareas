public class Main {
    public static void main(String[] args) {
        Bag<Integer> bag = new LinkedBag<>();
        bag.add(5);
        bag.add(3);
        bag.add(1);
        bag.add(2);
        bag.add(3);
        System.out.println(bag);
        //3,2,1,3,5
        bag.xchange(1,3);
        //3,3,1,2,5
        System.out.println(bag);
    }
}
