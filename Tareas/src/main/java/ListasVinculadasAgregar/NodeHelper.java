import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class NodeHelper {

    public static Node push(final Node head, final int data) {
        return new Node( data, head );
    }

    public static Node buildOneTwoThree() {
        return push( push( new Node( 3 ), 2 ), 1 );
    }

    public static Node build(int[] arr) {
        Node node = null;
        for ( int i = arr.length - 1; i > -1; i-- ) {
            node = push( node, arr[i] );
        }
        return node;
    }

    public static Node sortedInsert(Node head, int data) {
        if ( head == null || data < head.data ) {
            return new Node( data, head );
        }
        else {
            head.next = sortedInsert( head.next, data );
            return head;
        }
    }

    public static void assertEqual(Node a, Node b) {
        while ( a != null && b != null ) {
            assertEquals( a.data, b.data );
            a = a.next;
            b = b.next;
        }
        assertNull( a );
        assertNull( b );
    }
}