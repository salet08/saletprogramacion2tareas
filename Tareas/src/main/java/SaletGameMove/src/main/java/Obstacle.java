public class Obstacle {

    private int positionX;
    private int positionY;

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int randomNumY) {
        this.positionY = randomNumY;
    }
}
