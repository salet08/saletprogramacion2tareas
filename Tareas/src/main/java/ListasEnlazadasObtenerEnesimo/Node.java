public class Node {
    public int data;
    public Node next = null;

    public static int getNth(Node n, int index) throws Exception {
        if (index == 0) {
            return n.data;
        } else {
            return nextNode(n, index);
        }
    }

    public static int nextNode(Node n, int index) throws Exception {
        return getNth(n.next, index -1);
    }
}
