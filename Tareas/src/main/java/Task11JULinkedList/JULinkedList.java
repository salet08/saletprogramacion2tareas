import java.util.*;

public class JULinkedList<T> implements List<T> {

    public Nodo<T> raiz;
    private int longitud = 0;

    public JULinkedList() {
    }

    @Override
    public int size() {
        return this.longitud;
    }

    @Override
    public boolean isEmpty() {
        return raiz == null;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<>() {
            int counterIte = 0;
            Nodo<T> current = raiz;

            public boolean hasNext() {
                return size() > counterIte;
            }

            public T next() {

                if (counterIte > 0) {
                    current = current.sig;
                }
                counterIte++;
                return current.dato;
            }
        };
    }

    @Override
    public boolean add(T element) {
        Nodo<T> nodo;
        if (isEmpty()) {
            nodo = new Nodo<>(element);
        } else {
            nodo = new Nodo<>(element);
            nodo.sig = raiz;
        }
        raiz = nodo;
        longitud++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        Nodo<T> anterior = raiz;
        for (Nodo<T> actual = raiz; actual != null; anterior = actual, actual = actual.sig) {
            if (actual.dato.equals(o)) {
                if (raiz == actual) {
                    raiz = actual.sig;
                    return true;
                } else {
                    anterior.sig = actual.sig;
                    return true;
                }
            }
        }
        return false;
    }


    @Override
    public void clear() {
        this.raiz = null;
        this.longitud = 0;
    }

    @Override
    public T get(int index) {
        if (isEmpty() || index >= longitud || index < 0) {
            return null;
        }
        Nodo<T> nodo = raiz;
        for (int i = 0; i < index; i++, nodo = nodo.sig);
        return nodo.dato;
    }

    @Override
    public T remove(int index) {
        if (isEmpty() || index >= longitud || index < 0) {
            return null;
        }
        Nodo<T> auxNodo = raiz;
        if (index == 0) {
            raiz = raiz.sig;
        }
        Nodo<T> removed = raiz;
        for (int i = 0; auxNodo != null && i < index - 1; i++, removed = removed.sig) {
        }
        T datoRemoved = removed.sig.dato;
        removed.sig = removed.sig.sig;
        longitud--;
        return datoRemoved;
    }

    @Override
    public boolean contains(Object object) {
        if (isEmpty()) {
            return false;
        }else {
            for (Nodo<T> nodo = raiz; nodo != null; nodo = nodo.sig) {
                if (nodo.dato.equals(object)) {
                    return true;
                }
            }
            return false;
        }
    }

    @Override
    public T set(int index, T element) {
        if (!indexValidate(index)) return null;

        Nodo<T> current = getNodo(index);
        T previousData = current.getDato();
        current.setDato(element);
        return previousData;
    }

    public Nodo<T> getNodo(int index) {
        Nodo<T> nodo = raiz;
        for (int i = 0; i < index; i++) {
            nodo = nodo.sig;
        }
        return nodo;
    }

    private boolean indexValidate(int index){
        return !isEmpty() && index < size() && index >= 0;
    }

    @Override
    public int indexOf(Object o) {
        int count = -1;
        for (int i = 0; i < size(); i++){
            if (o.equals(get(i))){
                return i;
            }
        }
        return count;
    }

    @Override
    //ultimo indice de
    public int lastIndexOf(Object object) {
        for (int i = 0; i < this.size(); i++) {
            if (object.equals(get(i))) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void add(int index, T element) {
        if (isEmpty() && index == 0) {
            raiz = new Nodo<>(element);
            longitud++;

        } else if (indexValidate(index)) {
            Nodo<T> current = raiz;
            Nodo<T> newNodo = new Nodo<>(element);

            if (index == 0) {
                newNodo.setSig(raiz);
                raiz = newNodo;
            } else {
                for (int i = 0; i < index - 1; i++) {
                    current = current.getSig();
                }
                newNodo.setSig(current.getSig());
                current.setSig(newNodo);
            }
            longitud++;
        }
    }

/***********************************************************/

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public Object[] toArray() {

        return new Object[0];
    }
}
