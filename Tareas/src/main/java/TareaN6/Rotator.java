package TareaN6;

import java.util.Arrays;

public class Rotator {

    public Object[] rotate(Object[] data, int n) {
        if (n > 0) {
            timesToRotateRight(data, n);
        } else {
            timesToRotateLeft(data, n);
        }
        return data;
    }

    private void rotateLeft(Object[] data) {
        int temp = (int) data[0];
        for (int i = 0; i < data.length - 1; i++) {
            data[i] = data [i+1];
        }
        data[data.length-1] = temp;
        returnArray(data);
    }

    private void rotateRight(Object[] data) {
        int temp = (int) data[data.length - 1];
        for (int i = data.length - 1; i > 0; i--) {
            data[i] = data[i - 1];
        }
        data[0] = temp;
        returnArray(data);
    }

    private void timesToRotateLeft(Object[] data, int vueltas) {
        for (int i = 0; i > vueltas; i--) {
            rotateLeft(data);
        }
    }

    private void timesToRotateRight(Object[] data, int vueltas) {
        for (int i = 1; i <= vueltas; i++) {
            rotateRight(data);
        }
    }

    private String returnArray(Object[] data) {
        return Arrays.toString(data);
    }
}
